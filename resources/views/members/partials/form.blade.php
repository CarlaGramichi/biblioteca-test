<div class="row">

    <div class="col-md-12">

        <div class="card card-warning">

            <div class="card-body">

                <div class="form-group col-sm-6 ">

                    {!! Form::label('first_name', 'Nombre') !!}
                    {!! Form::text('first_name', null, ['class' => 'form-control', 'required']) !!}
                </div>

                <div class="form-group col-sm-6 ">

                    {!! Form::label('last_name', 'Apellido') !!}
                    {!! Form::text('last_name', null, ['class' => 'form-control', 'required']) !!}
                </div>

                <div class="form-group col-sm-6">
                    {!! Form::label('phone', 'Telefono') !!}
                    {!! Form::text('phone', null, ['class' => 'form-control','required']) !!}
                </div>

                <div class="form-group col-sm-3">
                    {!! Form::label('book_limit', 'Limite de libros') !!}
                    {!! Form::number('book_limit', null, ['class' => 'form-control','required']) !!}
                </div>

                <div class="form-group col-sm-3">
                    {!! Form::label('active', 'Estado') !!}
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" id="gridCheck" name="active" checked="checked" value=1>
                        <label class="form-check-label" for="gridCheck">
                            Activo
                        </label>
                    </div>

                </div>

            </div>
        </div>

    </div>
</div>

