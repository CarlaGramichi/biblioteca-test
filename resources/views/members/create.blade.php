@extends('adminlte::page')

@section('title', 'Socios')

@section('content')

    {!! Form::open(['route' => 'members.store', 'method'=>'post','class' => 'col-sm-12']) !!}

    <div class="row">
        <div class="mt-3 col-lg-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <h3 class="font-weight-bold">Nuevo Socio</h3>
                    <br>

                    @include('members.partials.form')

                    <div class="row">
                        <div class="col-sm-12">
                            <a href="{{ route('members.index') }}" type="button" class="btn btn-danger float-left">
                                <span class="fa fa-arrow-left"></span>&emsp;
                                Volver
                            </a>

                            <button type="submit" class="btn btn-primary float-right">
                                Guardar&emsp;
                                <span class="fa fa-save"></span>
                            </button>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    {!! Form::close() !!}

@endsection



