@extends('adminlte::page')

@section('title', 'Socios')
@section('content')
    <div class="row">
        <div class="mt-3 col-lg-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <h3 class="font-weight-bold">Socios</h3>
                </div>
                @include('partials.alerts')
                <div class="table-responsive">
                    <table id="users" class="table table-hover table-striped center">
                        <thead>
                        <tr class="text-center">
                            <th class="text-center">Id</th>
                            <th class="text-center">Nombre</th>
                            <th class="text-center">Apellido</th>
                            <th class="text-center">Telefono</th>
                            <th class="text-center">Estado</th>
                            <th class="text-center">Limite de libros</th>
                            <th class="text-center">Acción</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($members as $member)
                            <tr>
                                <td class="text-center">{{ $member->id }}</td>
                                <td class="text-center">{{ $member->first_name }}</td>
                                <td class="text-center">{{ $member->last_name }}</td>
                                <td class="text-center">{{ $member->phone }}</td>
                                <td class="text-center">
                                    @if($member->active == 1 )
                                        <span class="badge btn-success">Activo</span>
                                    @endif

                                    @if($member->active == 0 )
                                            <span class="badge btn-danger">Inactivo</span>
                                        @endif
                                </td>
                                <td class="text-center">{{ $member->book_limit }}</td>
                                <td class="text-center">
                                    <div class="btn-group" role="group" aria-label="Basic example">
                                        <a href="{{ route('members.edit',['member'=>$member->id]) }}" class="btn btn-primary btn-sm" data-toggle="tooltip" data-placement="left" title="Editar">
                                            <i class="fas fa-edit"></i>
                                        </a>
                                        <a href="{{ route('members.show',['member'=>$member->id]) }}" class="btn btn-warning btn-sm" data-toggle="tooltip" data-placement="top" title="Ver">
                                            <i class="fas fa-eye"></i>
                                        </a>
                                        <button class="btn btn-danger btn-sm" data-toggle="tooltip" data-placement="right" title="Eliminar" onclick="deleteMember({{$member->id}},'{{$member->first_name}}')">
                                            <i class="fas fa-trash"></i>
                                        </button>
                                    </div>


                                </td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
@endsection

@section('js')

    <script type="text/javascript">


        $(function () {
            $('[data-toggle="tooltip"]').tooltip()
        })

        function deleteMember(userId,userName){
            Swal.fire({
                title: '¿Estas seguro que deseas Eliminar el socio: '+userName+ '?',
                text: "¡No podrás reverir este cambio!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Sí, eliminar!',
                cancelButtonText: 'Cancelar'
            }).then((result) => {
                console.log(result)
                if (result.isConfirmed) {

                    $.ajax({

                        url: 'members/'+userId,
                        method: 'DELETE',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        success: function (result) {
                            Swal.fire(
                                'Eliminado!',
                                'El Usuario fue eliminado!',
                                'success'
                            ).then((result) => {
                                if (result.isConfirmed) {
                                    window.location.reload();
                                }
                            })

                        }
                    });

                }
            })
        }

    </script>
@endsection

