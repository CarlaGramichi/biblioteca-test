<div class="row">
    <div class="col-md-12">
        <div class="card card-warning">
            <div class="card-body">
                <div class="form-group col-sm-6 ">
                    {!! Form::label('first_name', 'Nombre') !!}
                    {!! Form::text('first_name', null, ['class' => 'form-control', 'required']) !!}
                </div>
                <div class="form-group col-sm-6 ">
                    {!! Form::label('last_name', 'Apellido') !!}
                    {!! Form::text('last_name', null, ['class' => 'form-control', 'required']) !!}
                </div>
            </div>
        </div>

    </div>
</div>

