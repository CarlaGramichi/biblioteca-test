@extends('adminlte::page')

@section('title', 'Autores')
@section('content')
    <div class="row">
        <div class="mt-3 col-lg-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <h3 class="font-weight-bold">Autores</h3>
                </div>
                @include('partials.alerts')


                <div class="table-responsive">
                    <table id="authors" class="table table-hover table-striped center">
                        <thead>
                        <tr class="text-center">
                            <th class="text-center">Id</th>
                            <th class="text-center">Nombre</th>
                            <th class="text-center">Apellido</th>
                            <th class="text-center">Acción</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($authors as $author)
                            <tr>
                                <td class="text-center">{{ $author->id }}</td>
                                <td class="text-center">{{ $author->first_name }}</td>
                                <td class="text-center">{{ $author->last_name }}</td>
                                <td class="text-center">
                                    <div class="btn-group" role="group" aria-label="Basic example">
                                        <a href="{{ route('authors.edit',['author'=>$author->id]) }}" class="btn btn-primary btn-sm" data-toggle="tooltip" data-placement="left" title="Editar">
                                            <i class="fas fa-edit"></i>
                                        </a>
                                        <a href="{{ route('authors.show',['author'=>$author->id])}}" class="btn btn-warning btn-sm" data-toggle="tooltip" data-placement="top" title="Ver">
                                            <i class="fas fa-eye"></i>
                                        </a>
                                        <button class="btn btn-danger btn-sm" data-toggle="tooltip" data-placement="right" title="Eliminar" onclick="deleteAuthor({{$author->id}},'{{$author->first_name}}')">
                                            <i class="fas fa-trash"></i>
                                        </button>
                                    </div>


                                </td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
@endsection

@section('js')

    <script type="text/javascript">


        $(function () {
            $('[data-toggle="tooltip"]').tooltip()
        })

        function deleteAuthor(userId,userName){
            Swal.fire({
                title: '¿Estas seguro que deseas Eliminar el socio: '+userName+ '?',
                text: "¡No podrás reverir este cambio!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Sí, eliminar!',
                cancelButtonText: 'Cancelar'
            }).then((result) => {
                console.log(result)
                if (result.isConfirmed) {

                    $.ajax({

                        url: 'authors/'+userId,
                        method: 'DELETE',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        success: function (result) {
                            Swal.fire(
                                'Eliminado!',
                                'El Usuario fue eliminado!',
                                'success'
                            ).then((result) => {
                                if (result.isConfirmed) {
                                    window.location.reload();
                                }
                            })

                        }
                    });

                }
            })
        }

    </script>
@endsection

