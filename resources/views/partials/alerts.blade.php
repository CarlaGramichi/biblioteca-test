<div class="col-12" style="flex: 0!important;">

    @if(count($errors))

            <ul class="list-unstyled">
                @foreach($errors->all() as $error)

                    <li>
                        <div class="alert alert-danger" role="alert" aria-label="Close">
                            <b>{{ $error }}</b>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </li>

                @endforeach

            </ul>

    @endif

    @if(session('success'))
        <div class="alert alert-success" role="alert" >
            {!! session('success') !!}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>

    @endif

</div>
