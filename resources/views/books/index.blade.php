@extends('adminlte::page')

@section('title', 'Libros')
@section('content')
    <div class="row">
        <div class="mt-3 col-lg-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <h3 class="font-weight-bold">Libros</h3>
                </div>
                @include('partials.alerts')
                <div class="table-responsive">
                    <table id="users" class="table table-hover table-striped center">
                        <thead>
                        <tr class="text-center">
                            <th class="text-center">Id</th>
                            <th class="text-center">Titulo</th>
                            <th class="text-center">Autor</th>
                            <th class="text-center">Total de Copias</th>
                            <th class="text-center">Total Disponibles</th>
                            <th class="text-center">Acción</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($books as $book)
                            <tr>
                                <td class="text-center">{{ $book->id }}</td>
                                <td class="text-center">{{ $book->title }}</td>
                                <td class="text-center">{{ $book->author->last_name }}, {{ $book->author->first_name }}</td>
                                <td class="text-center">{{ $book->copy->total }}</td>
                                <td class="text-center">{{ $book->copy->available }}</td>
                                <td class="text-center">
                                    <div class="btn-group" role="group" aria-label="Basic example">
                                        <a href="{{ route('books.edit',['member'=>$book->id]) }}" class="btn btn-primary btn-sm" data-toggle="tooltip" data-placement="left" title="Editar">
                                            <i class="fas fa-edit"></i>
                                        </a>
                                        <a href="{{ route('books.show',['member'=>$book->id]) }}" class="btn btn-warning btn-sm" data-toggle="tooltip" data-placement="top" title="Ver">
                                            <i class="fas fa-eye"></i>
                                        </a>
                                        <button class="btn btn-danger btn-sm" data-toggle="tooltip" data-placement="right" title="Eliminar" onclick="deleteBook({{$book->id}},'{{$book->title}}')">
                                            <i class="fas fa-trash"></i>
                                        </button>
                                    </div>


                                </td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
@endsection

@section('js')

    <script type="text/javascript">


        $(function () {
            $('[data-toggle="tooltip"]').tooltip()

            $('.select2').select2();

        })

        function deleteBook(userId,userName){
            Swal.fire({
                title: '¿Estas seguro que deseas Eliminar el socio: '+userName+ '?',
                text: "¡No podrás reverir este cambio!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Sí, eliminar!',
                cancelButtonText: 'Cancelar'
            }).then((result) => {
                if (result.isConfirmed) {

                    $.ajax({

                        url: 'books/'+userId,
                        method: 'DELETE',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        success: function (result) {
                            Swal.fire(
                                'Eliminado!',
                                'El Usuario fue eliminado!',
                                'success'
                            ).then((result) => {
                                if (result.isConfirmed) {
                                    window.location.reload();
                                }
                            })

                        }
                    });

                }
            })
        }

    </script>
@endsection

