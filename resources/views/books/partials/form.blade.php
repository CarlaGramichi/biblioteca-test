<div class="row">

    <div class="col-md-12">

        <div class="card card-warning">

            <div class="card-body">
                <div class="form-group col-sm-6 ">

                    {!! Form::label('title', 'Titulo') !!}
                    {!! Form::text('title', null, ['class' => 'form-control', 'required']) !!}
                </div>


                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label('author', 'Autor') !!}
                            {!! Form::select('authors', $authors, isset($book) ? $book->author_id : null, ['class' => 'form-control select2', 'style' => 'width: 100%;',  'id' => 'author','data-select2-id' => 'authors', 'placeholder' => 'Seleccionar un Autor']) !!}
                        </div>
                    </div>
                </div>


                <div class="form-group col-sm-6">
                    {!! Form::label('total', 'Total de Ejemplares') !!}
                    {!! Form::number('total', isset($book) ? $book->copy->total : null, ['class' => 'form-control','required']) !!}
                </div>
                <div class="form-group col-sm-6">
                    {!! Form::label('available', 'Ejemplares disponibles') !!}
                    {!! Form::number('available', isset($book) ? $book->copy->available : null, ['class' => 'form-control','required']) !!}
                </div>

            </div>
        </div>

    </div>
</div>


@section('js')
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet"/>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <script type="text/javascript">


        $(function () {

            $('.select2').select2();

        })
    </script>
@endsection

