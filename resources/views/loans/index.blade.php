@extends('adminlte::page')

@section('title', 'Prestamos')
@section('content')
    <div class="row">
        <div class="mt-3 col-lg-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <h3 class="font-weight-bold">Préstamos</h3>
                </div>
                @include('partials.alerts')
                <div class="table-responsive">
                    <table id="users" class="table table-hover table-striped center">
                        <thead>
                        <tr class="text-center">
                            <th class="text-center">Id</th>
                            <th class="text-center">Socio</th>
                            <th class="text-center">Libro</th>
                            <th class="text-center">Estado</th>
                            <th class="text-center">Observaciones</th>
                            <th class="text-center">Acción</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($loans as $loan)
                            <tr>
                                <td class="text-center">{{ $loan->id }}</td>
                                <td class="text-center">{{ $loan->member->first_name }}, {{ $loan->member->last_name }}</td>
                                <td class="text-center">{{ $loan->books->title }}</td>
                                <td class="text-center">
                                    @if($loan->status == 'En Proceso' )
                                        <span class="badge btn-warning"> {{ $loan->status }} </span>
                                    @endif

                                    @if($loan->status == 'Entregado' )
                                            <span class="badge btn-success">{{$loan->status}}</span>
                                        @endif
                                </td>
                                <td class="text-center">{{ $loan->observations}}</td>
                                <td class="text-center">
                                    <div class="btn-group" role="group" aria-label="Basic example">
                                        <a href="{{ route('loans.edit',['member'=>$loan->id]) }}" class="btn btn-primary btn-sm" data-toggle="tooltip" data-placement="left" title="Editar">
                                            <i class="fas fa-edit"></i>
                                        </a>
                                        <a href="{{ route('loans.show',['member'=>$loan->id]) }}" class="btn btn-warning btn-sm" data-toggle="tooltip" data-placement="top" title="Ver">
                                            <i class="fas fa-eye"></i>
                                        </a>
                                    </div>
                                </td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
@endsection

@section('js')

    <script type="text/javascript">


        $(function () {
            $('[data-toggle="tooltip"]').tooltip()
        })


    </script>
@endsection

