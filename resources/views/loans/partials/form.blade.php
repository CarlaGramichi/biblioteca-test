<div class="row">

    <div class="col-md-12">

        <div class="card card-warning">

            <div class="card-body">

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            {!! Form::label('member', 'Socio') !!}
                            {!! Form::select('member', $members, isset($loan) ? $loan->member->id : null, ['class' => 'form-control select2', 'style' => 'width: 100%;', 'placeholder' => 'Seleccionar un Socio']) !!}
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="form-group">
                            {!! Form::label('books', 'Libro') !!}
                            {!! Form::select('books', $books,  isset($loan) ? $loan->books->id : null, ['class' => 'form-control select2', 'style' => 'width: 100%;', 'data-select2-id' => 'books', 'placeholder' => 'Seleccionar un Libro']) !!}
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="form-group">
                            {!! Form::label('status', 'Estado') !!}
                            {!! Form::select('status', $status, null, ['class' => 'form-control select2', 'style' => 'width: 100%;', 'data-select2-id' => 'status', 'placeholder' => 'Seleccionar un Estado']) !!}
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="form-group">
                            {!! Form::label('observations', 'Observacines') !!}
                            {!! Form::textarea('observations', null, ['class' => 'form-control', 'rows' => 3]) !!}
                        </div>
                    </div>


                </div>
            </div>

        </div>
    </div>

    @section('js')
        <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet"/>
        <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
        <script type="text/javascript">


            $(function () {

                $('.select2').select2();
            })
        </script>
@endsection

