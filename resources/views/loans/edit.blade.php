@extends('adminlte::page')

@section('title', 'Prestamo')

@section('content')


    {!! Form::model($loan,['route' => ['loans.update',$loan], 'method'=>'put','class' => 'col-sm-12']) !!}

    <div class="row">
        <div class="mt-3 col-lg-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <h3 class="font-weight-bold">Editar Préstamos</h3>

                    @include('loans.partials.form')


                    <div class="row">
                        <div class="col-sm-12">
                            <a href="{{ route('loans.index') }}" type="button" class="btn btn-danger float-left">
                                <span class="fa fa-arrow-left"></span>&emsp;
                                Volver
                            </a>

                            <button type="submit" class="btn btn-primary float-right">
                                Guardar&emsp;
                                <span class="fa fa-save"></span>
                            </button>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

    {!! Form::close() !!}

@endsection

