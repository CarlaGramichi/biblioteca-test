<?php

use Illuminate\Database\Seeder;

class MembersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('members')->insert([
            'first_name' => 'Pedro',
            'last_name' => 'Lopez',
            'phone' => '3874890409',
            'book_limit' => 3,
            'active' => 1,
        ]);

        DB::table('members')->insert([
            'first_name' => 'Julieta',
            'last_name' => 'Rodriguez',
            'phone' => '38748990409',
            'book_limit' => 5,
            'active' => 1,
        ]);
    }
}
