<?php

use Illuminate\Database\Seeder;

class AuthorsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('authors')->insert([
            'first_name' => 'Pedro',
            'last_name' => 'Lopez',
        ]);

        DB::table('authors')->insert([
            'first_name' => 'Julieta',
            'last_name' => 'Rodriguez',
        ]);
        DB::table('authors')->insert([
            'first_name' => 'Roberto',
            'last_name' => 'Juarez',
        ]);

        DB::table('authors')->insert([
            'first_name' => 'Julieta',
            'last_name' => 'Lopez',
        ]);
    }
}
