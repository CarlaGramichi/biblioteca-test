<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Loan extends Model
{
    protected $fillable = ['id','book_id','member_id','status','observations'];



    public function setStatusAttribute($value)
    {
        $this->attributes['status'] = ucwords($value);
    }

    public function setObservationsttribute($value)
    {
        $this->attributes['observations'] = ucwords($value);
    }
    public function books()
    {
        return $this->hasOne(Book::class,'id','book_id');
    }

    public function member()
    {
        return $this->belongsTo(Member::class);
    }
}
