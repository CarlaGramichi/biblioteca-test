<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    protected $fillable = ['author_id','title'];

    public function setTitleAttribute($value)
    {
        $this->attributes['title'] = ucwords($value);
    }

    public function copy()
    {
        return $this->hasOne(Copy::class,'book_id','id');
    }
    public function author()
    {
        return $this->belongsTo(Author::class);
    }

}
