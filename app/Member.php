<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Member extends Model
{
    protected $fillable = ['first_name','last_name','phone','book_limit','active'];
    protected $appends = ['full_name'];
    public function setFirstNameAttribute($value)
    {
        $this->attributes['first_name'] = ucwords($value);
    }

    public function setLastNameAttribute($value)
    {
        $this->attributes['last_name'] = ucwords($value);
    }
    public function getFullNameAttribute()
    {
        return $this->attributes['first_name'] . ' ' . $this->attributes['last_name'];
    }
    public function loans()
    {
        return $this->belongsToMany(Book::class, 'loans', 'member_id', 'book_id')->withTimestamps();
    }
}
