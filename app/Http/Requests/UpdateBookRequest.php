<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateBookRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => ['required', 'string'],
            'authors' => ['required', 'string'],
            'total' => ['required', 'numeric','gt:0'],
            'available' => ['required', 'numeric','lte:total'],
        ];
    }

    public function messages(): array
    {
        return [
            'title.required' => 'Titulo: Campo requerido',
            'title.string' => 'Titulo: Debe ser texto',
            'authors.required' => 'Autor: Campo requerido',
            'total.required' => 'Total de Ejemplares: Campo requerido',
            'total.numeric' => 'Total de Ejemplares: Debe ser un número',
            'total.gt' => 'Total de Ejemplares: Debe ser un número mayor a 0',
            'available.required' => 'Ejemplares disponibles: Campo requerido',
            'available.numeric' => 'Ejemplares disponibles: Debe ser un número',
            'available.lte' => 'Ejemplares disponibles: No puede ser mayor al Total',
        ];

    }

}
