<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreMemberRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => ['required', 'string'],
            'last_name' => ['required', 'string'],
            'phone' => ['required', 'string'],
            'book_limit' => ['required', 'numeric'],
            'active' => ['sometimes'],
        ];
    }

    public function messages(): array
    {
        return [
            'first_name.required' => 'Nombre: Campo requerido',
            'first_name.string' => 'Nombre: Debe ser texto',
            'last_name.required' => 'Apellido: Campo requerido',
            'last_name.string' => 'Apellido: Debe ser texto',
            'phone.required' => 'Telefono: Campo requerido',
            'phone.string' => 'Telefono: Debe ser texto',
            'book_limit.numeric' => 'limite de libros: Debe ser numérico',
            'book_limit.required' => 'limite de libros: Campo requerido',
        ];

    }
}
