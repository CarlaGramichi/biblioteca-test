<?php

namespace App\Http\Controllers;

use App\Author;
use App\Book;
use App\Copy;
use App\Http\Requests\UpdateBookRequest;
use Illuminate\Http\Request;

class BookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $books = Book::get();
        return view('books.index', compact('books'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $authors = Author::all()->pluck('first_name','id');
        return view('books.create', compact('authors'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $book = new Book;
        $book->title = $request->title;
        $book->author_id = $request->authors[0];
        $book->save();

        $copies =  new Copy;
        $copies->available =  $request->available;
        $copies->total =  $request->total;
        $copies->book()->associate($book);
        $copies->save();


        return redirect()->route('books.index')->with('success', "Libro creado con éxito.");

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function show(Book $book)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function edit(Book $book)
    {

        $authors = Author::all()->pluck('first_name','id');
        return view('books.edit', compact('book','authors'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateBookRequest $request, Book $book)
    {

        $book->update([
            'title' => $request->title,
            'author_id' => $request->authors
        ]);

        $copy = Copy::where('book_id',$book->id);
        $copy->update([
            'available' => $request->available,
            'total' => $request->total
        ]);

        return redirect()->route('books.index')->with('success', "Libro actualizado con éxito.");

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function destroy(Book $book)
    {
        $book->copy()->delete();
        $book->delete();
    }
}
