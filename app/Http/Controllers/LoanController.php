<?php

namespace App\Http\Controllers;

use App\Book;
use App\Copy;
use App\Loan;
use App\Member;
use Illuminate\Http\Request;

class LoanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function index()
    {
        $loans = Loan::get();
        return view('loans.index',compact('loans'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $members = Member::where('active', 1)
            ->where('book_limit', '>', 0)
            ->get()
            ->map(function ($member) {
                return [
                    'full_name' => $member->first_name . ' ' . $member->last_name,
                    'id' => $member->id
                ];
            })
            ->pluck('full_name', 'id');

        $books = Book::whereHas('copy', function ($query) {
            $query->where('available', '>', 0);
        })
            ->get()
            ->pluck('title', 'id');

        $status = [
            'En proceso' => 'En proceso',
            'Entregado' => 'Entregado'
        ];

        return view('loans.create', compact('members', 'books', 'status'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Loan::create([
            'book_id' => $request->books,
            'member_id' => $request->member,
            'status' => $request->status,
            'observations' => $request->observations,
        ]);

        if ($request->status == 'En proceso') {

            $copy = Copy::where('book_id', $request->books)->first();
            $sumAvailable = $copy->available - 1;

            $copy->where('book_id', $request->books)
                ->update([
                    'available' => $sumAvailable
                ]);
        }

        return redirect()->route('loans.index')->with('success', "Préstamo creado con éxito.");

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Loan  $loan
     * @return \Illuminate\Http\Response
     */
    public function show(Loan $loan)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Loan  $loan
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function edit(Loan $loan)
    {
        $members = Member::where('active', 1)
            ->where('book_limit', '>', 0)
            ->get()
            ->map(function ($member) {
                return [
                    'full_name' => $member->first_name . ' ' . $member->last_name,
                    'id' => $member->id
                ];
            })
            ->pluck('full_name', 'id');

        $books = Book::whereHas('copy', function ($query) {
            $query->where('available', '>', 0);
        })
            ->get()
            ->pluck('title','id');

        $status = [
            'En proceso' => 'En proceso',
            'Entregado' => 'Entregado'
        ];
        return view('loans.edit',compact('members','books','status','loan'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Loan  $loan
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Loan $loan)
    {
        $loan->update([
            'book_id' => $request->books,
            'member_id' => $request->member,
            'status' => $request->status,
            'observations' => $request->observations,
        ]);

        if ($request->status == 'Entregado') {

            $copy = Copy::where('book_id', $request->books)->first();
            $sumAvailable = $copy->available + 1;

            $copy->where('book_id', $request->books)
            ->update([
                'available' => $sumAvailable
            ]);
        }

        return redirect()->route('loans.index')->with('success', "Préstamo editado con éxito.");

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Loan  $loan
     * @return \Illuminate\Http\Response
     */
    public function destroy(Loan $loan)
    {
        //
    }
}
